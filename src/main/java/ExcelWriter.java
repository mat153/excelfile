import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

public class ExcelWriter {

    void writeExcelFile(Map<Integer,Map<Integer,String>> data2,Map<Integer,Short> data3,Map<Integer,Map<Integer,String>> data4 ) {
        Workbook work = new XSSFWorkbook();
        Sheet sheet = work.createSheet("Numbers");

        for (int i = 0; i < data2.size(); i++) {
            Row row = sheet.createRow(i);
            for (int j = 0; j < data3.get(0); j++) {
                int num = Integer.parseInt(data4.get(i).get(j));

                row.createCell(j).setCellValue(num);
/*
                row.createCell(j).setCellValue(data4.get(i).get(j));
*/

            }
        }

        try (FileOutputStream fileOut = new FileOutputStream("numbers.xlsx")) {
            work.write(fileOut);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
