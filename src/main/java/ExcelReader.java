import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

public class ExcelReader {

    Map<Integer, List<String>> data = new HashMap<>();
    Map<Integer,Map<Integer,String>> data2 = new HashMap<>();
    Map<Integer,Short> data3 = new HashMap<>();
    Map<Integer,Map<Integer,String>> data4 = new HashMap<>();

    Scanner scanner = new Scanner(System.in);

    String filePath = scanner.nextLine();

    FileInputStream inputStream = new FileInputStream(new File(filePath));
    Workbook workbook = new XSSFWorkbook(inputStream);

    Sheet sheet = workbook.getSheetAt(0);

    void read(){
        int i =0;
        for(Row row :sheet){
            data3.put(i,row.getLastCellNum());
            data2.put(i, new HashMap<>());
            for (int j = 0;j<row.getLastCellNum();j++){
                Cell cell = row.getCell(j);
                data2.get(i).put(j,cell.getStringCellValue());
            }
            i++;
        }
        /*System.out.println(data2);*/
    }

    void forRead(){
        /*System.out.println(data2.size());*/
        /*System.out.println("data 3" + data3);*/

        System.out.println("Podaj ilosc liczb w skali");
        Scanner scanner1 = new Scanner(System.in);
        Integer howMuch = scanner1.nextInt();

        System.out.println(howMuch);

        ArrayList<String> words = new ArrayList<>();
        ArrayList<String> numbers = new ArrayList<>();

        System.out.println("Podaj slowo do skali");

        for(int i=0;i<howMuch;i++)
        {
            Scanner scanner2 =new Scanner(System.in);
            words.add(scanner2.nextLine());
            numbers.add(String.valueOf(i+1));
        }

        System.out.println(numbers);

        System.out.println(words);

        for(int i=0;i < data2.size();i++){
            data4.put(i,new HashMap<>());
            for(int j=0;j<data3.get(0);j++){
                //data3 pomyslec jak dodac jak w ktoryms bedzie wiecej komorek
                /*System.out.println(data2.get(i).get(j));*/

                for (int k=0;k<howMuch;k++)
                {
                    if(data2.get(i).get(j).equals(words.get(k)))
                    {
                        data4.get(i).put(j,numbers.get(k));
                    }
                }
            }
        }
    }

    void readExcelFile(){
        int i = 0;
        for (Row row : sheet) {
            data.put(i, new ArrayList<String>());
            for (Cell cell : row) {
                switch (cell.getCellTypeEnum()) {
                    case STRING: data.get(i).add(cell.getRichStringCellValue().getString());
                        break;
                    case NUMERIC: if (DateUtil.isCellDateFormatted(cell)) {
                        data.get(i).add(cell.getDateCellValue() + "");
                    } else {
                        data.get(i).add(cell.getNumericCellValue() + "");
                    } break;
                    case BOOLEAN: data.get(i).add(cell.getBooleanCellValue() + ""); break;
                    case FORMULA: data.get(i).add(cell.getCellFormula() + ""); break;
                    default: data.get(i).add(" ");
                }
            }
            i++;
        }
        System.out.println(data);
        System.out.println(data.get(0));
    }


    String getFilePath()
    {
        return filePath;
    }

    public Map<Integer, Map<Integer, String>> getData4() {
        return data4;
    }

    public void setData4(Map<Integer, Map<Integer, String>> data4) {
        this.data4 = data4;
    }

    public Map<Integer, Short> getData3() {
        return data3;
    }

    public void setData3(Map<Integer, Short> data3) {
        this.data3 = data3;
    }

    public Map<Integer, Map<Integer, String>> getData2() {
        return data2;
    }

    public void setData2(Map<Integer, Map<Integer, String>> data2) {
        this.data2 = data2;
    }

    public ExcelReader() throws IOException {
    }
}
